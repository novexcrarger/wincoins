    <?php
        include ('header.html')
    ?>

<div class="equipment wrapper">
    <div class="left-sidebar">
        <?php

        include ('left-menu.html')

        ?>
    </div>
    <div class="main-content">
        <div class="content">
            <div class="col-xs-12 title">
                <h2>Покупка оборудования</h2>
            </div>

            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-3">
                        <p>У вас имеется <span>1</span> товар в корзине</p>
                    </div>
                    <div class="col-md-3">
                        <a href="#" class="link-to-basket">Перейти в корзину</a>
                    </div>
                </div>
            </div>





            <div class="col-md-4">
                <div class="cell">
                    <p class="name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</p>
                    <p class="term">Срок окупаемости: 14 месяцев</p>
                    <p class="old-price">324 000,00 (без НДС)</p>
                    <p class="price">270 000,00 </p>
                    <p class="nds">(без НДС)</p>
                    <button class="btn-blue">ЗАКАЗАТЬ</button>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cell">
                    <p class="name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</p>
                    <p class="term">Срок окупаемости: 14 месяцев</p>
                    <p class="old-price">324 000,00 (без НДС)</p>
                    <p class="price">270 000,00 </p>
                    <p class="nds">(без НДС)</p>
                    <button class="btn-blue">ЗАКАЗАТЬ</button>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cell">
                    <p class="name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</p>
                    <p class="term">Срок окупаемости: 14 месяцев</p>
                    <p class="old-price">324 000,00 (без НДС)</p>
                    <p class="price">270 000,00 </p>
                    <p class="nds">(без НДС)</p>
                    <button class="btn-blue">ЗАКАЗАТЬ</button>
                </div>
            </div>

            <div class="text-area col-xs-12">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Срок поставки оборудования с растаможкой до 30 календарных дней, с момента оплаты заказа.
                        </p>
                        <p>
                            Монтаж и пусконаладка 10 дней с момента поступления оборудования в дата-центр.
                        </p>
                        <p>
                            Выплаты вознаграждений от майнинга производятся автоматически на криптосчета владельца серверов каждые три часа.

                        </p>
                    </div>

                    <div class="col-md-6">
                        <div class="links">
                            <p><a href="#">Договор-оверта на покупку оборудования</p>
                            <p><a href="#">Договор-оверта на размещение оборудования в дата-центре</a></p>
                        </div>

                    </div>
                </div>

            </div>
        </div>



    </div>



</div>
    <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>

</html>