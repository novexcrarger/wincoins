
ymaps.ready(init);

function init () {
    var myMap = new ymaps.Map("map", {
            center: [59.082234, 37.907301],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        }),
        // Создание макета содержимого хинта.
        // Макет создается через фабрику макетов с помощью текстового шаблона.
        HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
            "<b>{{ properties.object }}</b><br />" +
            "{{ properties.address }}" +
            "</div>", {
                // Определяем метод getShape, который
                // будет возвращать размеры макета хинта.
                // Это необходимо для того, чтобы хинт автоматически
                // сдвигал позицию при выходе за пределы карты.
                getShape: function () {
                    var el = this.getElement(),
                        result = null;
                    if (el) {
                        var firstChild = el.firstChild;
                        result = new ymaps.shape.Rectangle(
                            new ymaps.geometry.pixel.Rectangle([
                                [0, 0],
                                [firstChild.offsetWidth, firstChild.offsetHeight]
                            ])
                        );
                    }
                    return result;
                }
            }
        );

    var myPlacemark = new ymaps.Placemark([59.082234, 37.907301], {
        address: 'ЖК: Клубный дом \"Рублевский\"',
        object: "Адрес: ул. Лесная,12 Клубный дом \"Рублевский\"\n",
        balloonContentHeader: "",
        balloonContentBody: "ЖК: Клубный дом \"Рублевский\"\n" +
        "Адрес: ул. Лесная,12 Клубный дом \"Рублевский\"\n" +
        "1 к.кв. от 40.59 до 46.86 кв.м\n" +
        "2 к.кв. от 61.53 до 70.69 кв.м\n" +
        "3 к.кв. от 75.12 до 107.71 кв.м",
        balloonContentFooter: ""
    }, {
        hintLayout: HintLayout,
        iconLayout: 'default#image',
        iconImageClipRect: [[0,0], [16, 27]],
        iconImageHref: '../svg/1.svg',
        iconImageSize: [45, 47],
        iconImageOffset: [-15, -27]
    });

    myMap.geoObjects.add(myPlacemark);


}


