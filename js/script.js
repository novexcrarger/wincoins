//настройка Owl Carousel
$(".owl-carousel").owlCarousel({
    loop: true,
    margin: 30,
    nav: false,
    dots: false,
    items: 1,
    lazyLoad: true,
    autoplay: false,
    mouseDrag: true,
    touchDrag: true
});

//Делегируем события кнопок next prev по умолчанию нашим кнопкам, которые могут находится ыне контейнера слайдера
var owl=$(".slider1");
owl.owlCarousel();
//$(".next") - находим нашу кнопку
$(".btn-next1").click(function(){
    owl.trigger("next.owl.carousel");
});
$(".btn-prev1").click(function(){
    owl.trigger("prev.owl.carousel");
});

var owl2=$(".slider2");
owl2.owlCarousel();
//$(".next") - находим нашу кнопку
$(".btn-next2").click(function(){
    owl2.trigger("next.owl.carousel");
});
$(".btn-prev2").click(function(){
    owl2.trigger("prev.owl.carousel");
});

Morris.Line({
    //Контейнер для вывода графика
    element: 'myfirstchart',
    //Данные для графика
    data: [
        { y: '2008', ruble: 24.48 },
        { y: '2009', ruble: 28.26 },
        { y: '2010', ruble: 29.59 },
        { y: '2011', ruble: 30.60 },
        { y: '2012', ruble: 31.22 },
        { y: '2013', ruble: 30.42 },
        { y: '2014', ruble: 33.15 },
        { y: '2015', ruble: 56.49 }

    ],
    //Массив занчений для оси X
    xkey: 'y',

    //Префикс в конце для оси Y
    postUnits:' $',

    //Цвет линий
    lineColors:['#4239DE'],
    //Выводимые линии
    ykeys: ['ruble'],
    //Названия линий
    labels: ['Wc/Rub']

});

/*Калькулятор */
/*Инициализация слайдеров*/
var oneSlider = document.getElementById("slider-1");
var twoSlider = document.getElementById("slider-2");
var threeSlider = document.getElementById("slider-3");

noUiSlider.create(oneSlider, {
    animate: true,
    animationDuration: 300,
    start: [5000],
    step: 100000,
    range: {
        min: [0],
        max: [500000]
    },
    pips: {
        mode: 'steps',
        stepped: true,
        density: 10
    },
    tooltips: true
});
noUiSlider.create(twoSlider, {
    animate: true,
    animationDuration: 300,
    start: [5],
    step: 5,
    range: {
        min: [5],
        max: [90]
    },
    pips: {
        mode: 'steps',
        stepped: true,
        density: 10
    },
    tooltips: true
});
noUiSlider.create(threeSlider, {
    animate: true,
    animationDuration: 300,
    start: [100],
    step: 10,
    range: {
        min: [100],
        max: [300]
    },
    pips: {
        mode: 'steps',
        stepped: true,
        density: 10
    },
    tooltips: true
});

/*переменные для полей вывода чисел*/
var score = document.getElementById("score");
var scoreAll = document.getElementById("score-all");
var fine = document.getElementById("fine");

/*полученние значений с ползунков*/
var scoreVal,scoreAllVal,fineVal;

function getElemantValue() {
    valueMoney = oneSlider.noUiSlider.get();
    valueDate = twoSlider.noUiSlider.get();
    valueStore = threeSlider.noUiSlider.get();
    scoreVal = (valueMoney * (valueDate/500));
    scoreVal = scoreVal.toFixed();
    scoreAllVal = scoreVal * valueStore;
    scoreAllVal = scoreAllVal.toFixed();
    fineVal = (scoreAllVal * 10000 * 1.11);
    fineVal = fineVal.toFixed();
}

/*вывод значений с ползунков*/

oneSlider.noUiSlider.on("update", function(values, handle) {
    getElemantValue();
    score.innerHTML = scoreVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    scoreAll.innerHTML = scoreAllVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    fine.innerHTML = fineVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
});

twoSlider.noUiSlider.on("update", function(values, handle) {
    getElemantValue();
    score.innerHTML = scoreVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    scoreAll.innerHTML = scoreAllVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    fine.innerHTML = fineVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
});

threeSlider.noUiSlider.on("update", function(values, handle) {
    getElemantValue();
    score.innerHTML = scoreVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    scoreAll.innerHTML = scoreAllVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
    fine.innerHTML = fineVal.toString(10).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
});

