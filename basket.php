<?php
include('header.html')
?>

    <div class="basket">
        <div class="container">
            <div class="row">
                <h2 class="text-center col-xs-12">
                    Корзина
                </h2>
                <table>
                    <tbody>
                    <tr class="table-head">
                        <td class="cell-name">Наименование</td>
                        <td>Цена (без НДС)</td>
                        <td>Количество</td>
                        <td>Стоимость</td>
                        <td></td>
                    </tr>
                    <tr class="content">
                        <td class="cell-name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</td>
                        <td class="price">270 000,00 <span>Р</span></td>
                        <td>
                            <button>-</button>
                            <input type="text" value="1">
                            <button>+</button>
                        </td>
                        <td class="price">270 000,00 <span>Р</span></td>
                        <td><a href="">Удалить</a></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-right col-xs-12">
                    <p class="itog-price">Итоговая стоимость: <span>270 000,00 <span>Р</span></span></p>
                </div>

                <div class="col-xs-12">
                    <h2 class="text-center">
                        Оформление заказа
                    </h2>
                    <div class="ordering-block">
                        <div class="ordering">
                            <p class="punkt">1. Личные данные</p>
                            <div class="form">
                                <input type="text" class="input-name" placeholder="ФИО">
                                <input type="text" class="input-tel" placeholder="Телефон">
                                <input type="text" class="input-email" placeholder="Email">
                            </div>
                            <p class="punkt">2. Выберите способ оплаты</p>
                            <div class="form-rgoup">
                                <label>
                                    <input class="radio" type="radio" value="1" name="oplata" checked>
                                    <span class="radio-custom"></span>Банковские карты
                                    <span>
                                        <img src="svg/33.svg" alt="">
                                        <img src="svg/34.svg" alt="">
                                        <img src="svg/35.svg" alt="">
                                </span>
                                </label>

                            </div>
                            <div class="form-rgoup">

                                <label>
                                    <input class="radio" type="radio" value="1" name="oplata">
                                    <span class="radio-custom"></span>Яедекс.Деньки
                                    <span>
                                    <img src="svg/ya.svg" alt="">
                                </span>
                                </label>
                            </div>
                        </div>
                        <div class="block-itog">
                            <p class="itog">Итого <span>270 000,00 <span>Р</span></span></p>
                            <button class="btn-orange">Оформить заказ</button>
                            <p>Нажимая "Оформить заказ" вы даёте согласие на хранение и обработку ваших персональных
                                данных в соответствии с <span>условиями</span></p>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>

<?php
include('footer.html')
?>