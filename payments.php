    <?php
        include ('header.html')
    ?>

<div class="payments">
    <div class="left-sidebar">
        <?php

        include ('left-menu.html')

        ?>
    </div>
    <div class="main-content">
        <div class="content">
            <div class="col-xs-12 title">
                <h2>Мои выплаты</h2>
            </div>

            <div class="col-md-7">
                <h3 class="subtitle">График изменений курса 1 WinCoin к российскому рублю</h3>
                <div class="filter">
                    <a href="" class="active">Неделя</a>
                    <a href="">Месяц</a>
                    <a href="">Год</a>
                </div>
                <div id="myfirstchart" class="desk"></div>
            </div>
            <div class="col-md-5">
                <h3 class="subtitle">Динамика стоимости 1 WinCoin в рублях</h3>
                <div class="filter">
                    <a href="" class="active">Неделя</a>
                    <a href="">Месяц</a>
                    <a href="">Год</a>
                </div>
                    <table>
                        <tr class="string-title">
                            <td>Дата</td>
                            <td>Российский рубль</td>
                        </tr>
                        <tr>
                            <td>Июль' 25/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 26/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 27/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 28/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 29/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 27/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 28/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 29/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                    </table>

            </div>
            <div class="col-xs-12">
                <button class="btn-orange">Получить выплату</button>
            </div>
        </div>



    </div>



</div>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/owl.carousel/dist/owl.carousel.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="bower_components/morrisjs/morris.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>