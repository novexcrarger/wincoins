    <?php
        include ('header.html')
    ?>
    <link rel="stylesheet" href="css/calc.css">
<div class="payments purchase">
    <div class="left-sidebar">
        <?php

        include ('left-menu.html')

        ?>
    </div>
    <div class="main-content">
        <div class="content">
            <div class="col-xs-12 title">
                <h2>Мои выплаты</h2>
            </div>

            <div class="col-md-7">
                <h3 class="subtitle">График изменений курса 1 WinCoin к российскому рублю</h3>
                <div class="filter">
                    <a href="" class="active">Неделя</a>
                    <a href="">Месяц</a>
                    <a href="">Год</a>
                </div>
                <div id="myfirstchart" class="desk"></div>
            </div>
            <div class="col-md-5">
                <h3 class="subtitle">Динамика стоимости 1 WinCoin в рублях</h3>
                <div class="filter">
                    <a href="" class="active">Неделя</a>
                    <a href="">Месяц</a>
                    <a href="">Год</a>
                </div>
                    <table>
                        <tr class="string-title">
                            <td>Дата</td>
                            <td>Российский рубль</td>
                        </tr>
                        <tr>
                            <td>Июль' 25/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 26/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 27/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 28/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 29/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 27/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 28/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                        <tr>
                            <td>Июль' 29/17</td>
                            <td>152 146.43 RUB</td>
                        </tr>
                    </table>

            </div>

            <div class="col-xs-12">
                <h3 class="subtitle">Калькулятор расчета</h3>


<!--                <div class="seo">-->
<!--
<!--
<!--
<!--
<!--                    <div class="section-return-info lightThis">-->
<!--                        <h1>Займов с одного магазина с учетом конверсии</h1>-->
<!--                        <h2 id="score"></h2>-->
<!--                    </div>-->
<!--                    <div class="section-heading">-->
<!--                        <h1>Количество магазинов</h1>-->
<!--                    </div>-->
<!--                    <div class="slider">-->
<!--                        <div class="slider-block">-->
<!--                            <div id="slider-3"></div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="section-return-info lightThis">-->
<!--                        <h1>Займов со всех магазинов</h1>-->
<!--                        <h2 id="score-all"></h2>-->
<!--                    </div>-->
<!--                    <div class="section-return-info">-->
<!--                       <h1>Средний чек - 10 000 рублей и Surplus - 111%</h1>-->
<!--                    </div>-->
<!--                    <div class="section-return-info lightThisFinal">-->
<!--                        <h1>Итого</h1>-->
<!--                        <h2 id="fine"></h2>-->
<!--                    </div>-->
<!--                </div>-->



                <div class="seo">
                    <div class="slider">
                        <div class="row">
                            <div class="col-md-7">
                                <p class="calc-title">Сумма вложений</p>
                                <div class="slider">
                                    <div class="slider-block">
                                        <div id="slider-1"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <p class="calc-title">На срок</p>
                                <div class="slider">
                                    <div class="slider-block">
                                        <div id="slider-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="section-return-info">
                        <div class="cell">
                            <span><img src="svg/1.svg" alt=""></span>
                            <span>Текущий курс:</span>
                            <span class="return-value">1<span>BTC</span> = 153682 <img src="svg/18.svg" alt=""></span>
                        </div>
                        <div class="cell-right">
                            <span><img src="svg/2.svg" alt=""></span>
                            <span>Вы заработаете:</span>
                            <span class="result">56 000</span>
                            <img class="svg19" src="svg/19.svg" alt="">
                        </div>
                       <div class="cell-right">
                           <span><img src="svg/3.svg" alt=""></span>
                           <span>К дате:</span>
                           <span class="result">19 июня</span>
                       </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <button class="btn-orange">Купить</button>
            </div>
        </div>





    </div>



</div>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/owl.carousel/dist/owl.carousel.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/9.2.0/nouislider.js"></script>
    <script src="bower_components/morrisjs/morris.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>