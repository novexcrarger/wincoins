    <?php
    include ('header.html')
    ?>

<section class="reviews">

    <div class="container">
        <div class="content">
            <div class="row">
                <div class="text-center title">
                    <h2>Отзывы</h2>
                </div>

                <p class="add-review">
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="14px" height="15px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"
                                 viewBox="0 0 516 570"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                 <g id="Слой_x0020_1">
                                  <metadata id="CorelCorpID_0Corel-Layer"/>
                                  <g id="_2461016289872">
                                   <path fill="#9E9C9C" fill-rule="nonzero" d="M492 24c-15,-15 -35,-24 -57,-24 -22,0 -42,9 -58,24 -5,5 -5,15 0,20 6,6 15,6 21,0 10,-10 23,-15 37,-15 14,0 27,5 37,15 20,21 20,54 0,74 0,1 -1,1 -1,1 0,1 -1,1 -1,1l-22 22 -121 -121c-6,-5 -15,-5 -20,0l-186 186c-6,5 -6,14 0,20 3,3 7,4 10,4 4,0 8,-1 10,-4l176 -175 16 16 -281 282c-3,2 -5,6 -5,10 0,4 2,7 5,10l9 9 -40 40c-3,3 -5,7 -5,10 0,4 2,8 5,11l17 17 -34 35c-3,2 -4,6 -4,10l0 0c0,4 1,7 4,10 3,3 7,4 10,4 4,0 8,-1 11,-4l34 -34 18 18c2,2 6,4 10,4 4,0 7,-2 10,-4l40 -41 9 10c3,2 7,4 10,4 4,0 8,-2 11,-4l323 -324c1,-1 1,-1 1,-1 1,-1 1,-1 1,-1 32,-32 32,-84 0,-115zm-405 441l-36 -36 30 -30 18 18 18 18 -30 30zm69 -31l-9 -9 -28 -28 -37 -37 272 -272 74 75 -272 271z"/>
                                   <path fill="#9E9C9C" fill-rule="nonzero" d="M476 541l-462 0c-8,0 -14,7 -14,15 0,8 6,14 14,14l462 0c8,0 14,-6 14,-14 0,-8 -6,-15 -14,-15z"/>
                                  </g>
                                 </g>
                            </svg>
                        </span> <a href="#" data-toggle="modal" data-target="#modalAddReview">Оставить отзыв</a></p>

                <div class="col-md-6">
                    <div class="row">
                        <div class="desk comment-desk">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="photo">
                                        <img src="images/system/2.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="title">
                                        <p>
                                            <span class="name">Alex,</span> VP of business development<br>
                                            <span class="from">from Innosilicon</span>
                                        </p>
                                    </div>
                                    <p>
                                        Genesis Mining является одной из лидирующих компаний облачного майнинга и нашим надежным партнером. Приятно видеть, что они являются порядочным сервисом облачного майнинга, который открыто демонстрирует свои майнинг-фермы широкой общественности.
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="desk comment-desk">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="photo">
                                        <img src="images/system/2.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="title">
                                        <p>
                                            <span class="name">Alex,</span> VP of business development<br>
                                            <span class="from">from Innosilicon</span>
                                        </p>
                                    </div>
                                    <p>
                                        Один из самых первых в мире производитель ASIC-чипов по 28-нм техпроцессу для BTC и LTC, Innosilicon выбирает Genesis Mining в качестве бизнес-партнёра в индустрии облачного майнинга за их честность, превосходный ориентированный на клиента сервис и отличный дизайн пользовательского интерфейса. Genesis Mining является лучшим в своём классе сервисом облачного майнинга, который использует наше технологически совершенное оборудование для майнинга. Эта уникальная синергия обеспечивает лучшее впечатление для тех, кто заинтересован в майнинге, и мы ожидаем долгих и благоприятных взаимоотношений.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="desk comment-desk">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="photo">
                                        <img src="images/system/2.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="title">
                                        <p>
                                            <span class="name">Alex,</span> VP of business development<br>
                                            <span class="from">from Innosilicon</span>
                                        </p>
                                    </div>
                                    <p>
                                        Genesis Mining является одним из наших крупнейших клиентов, благонадёжным и заслуживающим доверие партнером. Их подходящие места для майнинга и высококачественная инфраструктура программного обеспечения совместно с нашим оборудованием для майнинга высокого класса дают результат в виде великолепного и уникального продукта, а также в виде приятного впечатления для каждого, кто заинтересован в майнинге!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="desk comment-desk">

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="photo">
                                        <img src="images/system/2.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="title">
                                        <p>
                                            <span class="name">Alex,</span> VP of business development<br>
                                            <span class="from">from Innosilicon</span>
                                        </p>
                                    </div>
                                    <p>
                                        Genesis Mining является одним из наших крупнейших клиентов, благонадёжным и заслуживающим доверие партнером. Их подходящие места для майнинга и высококачественная инфраструктура программного обеспечения совместно с нашим оборудованием для майнинга высокого класса дают результат в виде великолепного и уникального продукта, а также в виде приятного впечатления для каждого, кто заинтересован в майнинге!
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="desk comment-desk">

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="photo">
                                        <img src="images/system/2.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="title">
                                        <p>
                                            <span class="name">Alex,</span> VP of business development<br>
                                            <span class="from">from Innosilicon</span>
                                        </p>
                                    </div>
                                    <p>
                                        В MinerEU удовлетворены таким надежным партнером, каким является Genesis Mining. Мы уже рекомендовали Genesis Mining тысячам наших клиентов, которые счастливы и довольны их превосходными услугами и продуктами.
                                    </p>
                                </div>
                            </div>


                        </div>
                        <div class="desk comment-desk">

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="photo">
                                        <img src="images/system/2.jpg">
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="title">
                                        <p>
                                            <span class="name">Alex,</span> VP of business development<br>
                                            <span class="from">from Innosilicon</span>
                                        </p>
                                    </div>
                                    <p>
                                        Genesis Mining является одной из лидирующих компаний облачного майнинга и нашим надежным партнером. Приятно видеть, что они являются порядочным сервисом облачного майнинга, который открыто демонстрирует свои майнинг-фермы широкой общественности.
                                    </p>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>










    </div>



</section>

    <?php
    include ('footer.html')
    ?>