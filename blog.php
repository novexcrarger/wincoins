    <?php
        include ('header.html')
    ?>

<section class="blog">
        <div class="title text-center col-xs-12">
            <h2>Наше оборудование</h2>
        </div>

    <div class="blog-row">
        <div class="image">
            <img src="images/content/blog/7.jpg">
        </div>
        <div class="desk preview-new">

            <div class="col-xs-12">
                <div class="title">
                    <p>
                        <span class="name">Биткойны: технология, которая будет жить вечно</span><span class="date">/17.01.2017</span>
                    </p>
                </div>
                <p>
                    Как и другие любители биткойнов, я обожаю говорить на эту тему. Я говорю об этом с пассажирами, которые летят со мной в самолете, с официантами, которые обслуживают меня в ресторанах. Не то, чтобы я намеренно...
                </p>
                <a class="more" href="#">Читать далее</a>
            </div>

        </div>
    </div>

    <div class="blog-row">
        <div class="image">
            <img src="images/content/blog/8.jpg">
        </div>
        <div class="desk preview-new">

            <div class="col-xs-12">
                <div class="title">
                    <p>
                        <span class="name">Биткойны: технология, которая будет жить вечно</span><span class="date">/17.01.2017</span>
                    </p>
                </div>
                <p>
                    Это был потрясающий год! 2016 год стал годом отличных новостей в мире криптовалюты: продолжилась инновация блокчейнов; биткойны продолжают развиваться и получать в мире все большее распространение; и, конечно же, мы увидели...
                </p>
                <a class="more" href="#">Читать далее</a>
            </div>

        </div>
    </div>

    <div class="blog-row">
        <div class="image">
            <img src="images/content/blog/9.jpg">
        </div>
        <div class="desk preview-new">

            <div class="col-xs-12">
                <div class="title">
                    <p>
                        <span class="name">Биткойны: технология, которая будет жить вечно</span><span class="date">/17.01.2017</span>
                    </p>
                </div>
                <p>
                    Последние новости из мира биткойнов готовились уже четыре года: Bitcoin ETF от братьев Винклевосс скоро будет принят или отвергнут Государственной комиссией по ценным бумагам и фондовому рынку (SEC). Это решение войдет в историю развития биткойнов...
                </p>
                <a class="more" href="#">Читать далее</a>
            </div>

        </div>
    </div>

    <div class="blog-row">
        <div class="image">
            <img src="images/content/blog/10.jpg">
        </div>
        <div class="desk preview-new">

            <div class="col-xs-12">
                <div class="title">
                    <p>
                        <span class="name">Биткойны: технология, которая будет жить вечно</span><span class="date">/17.01.2017</span>
                    </p>
                </div>
                <p>
                    По мере распространения биткойнов как валюты важно знать, как работает и как используется эта криптовалюта. Здесь мы объясняем, как работает майнинг биткойнов, и как он используется на практике...
                </p>
                <a class="more" href="#">Читать далее</a>
            </div>

        </div>
    </div>

    <div class="col-xs-12 text-center">
        <ul class="pagination">
            <li class="disabled"><a href="#">«</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">10</a></li>
            <li><a href="#">»</a></li>
        </ul>
    </div>



</section>

    <?php
        include ('footer.html')
    ?>