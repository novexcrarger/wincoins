<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Wincoin</title>
    <link rel="shortcut icon" href="favicon.png">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="bower_components/owl.carousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="bower_components/owl.carousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style.css"/>
    <link rel="stylesheet" href="css/responsive.css"/>
</head>
<body>

<div class="homepage">

    <section class="block-start">
        <div class="header">
            <div class="container">
                <div class="navbar-brand">
                    <a href="/"><img src="images/system/logo.png"></a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#menu-collapse"
                            aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Меню</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="menu-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="navbar-hide"><a href="/">ГЛАВНАЯ</a></li>
                        <li class="navbar-hide"><a href="about.php">О КОМПАНИИ</a></li>
                        <li class="navbar-hide"><a href="#">ПОЛЕЗНАЯ ИНФОРМАЦИЯ</a></li>
                        <li class="navbar-hide"><a href="contacts.php">КОНТАКТЫ</a></li>
                        <button class="btn-white">ВОЙТИ</button>
                        <button class="btn-white">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                        <button class="btn-basket"></button>
                    </ul>


                </div>
            </div>

        </div>
        <div class="text-center content">
            <h1 class="title">
                ПРОДАЖА УНИКАЛЬНОГО ОБОРУДОВАНИЯ ДЛЯ<br><span>ДОБЫЧИ КРИПТОВАЛЮТ</span>
            </h1>
            <button class="btn-orange">ПОДРОБНЕЕ <span></span></button>
        </div>
    </section>

    <section class="block-equipment">
        <div class="container">
            <div class="text-center title">
                <h2>Наше оборудование</h2>
            </div>

            <div class="cells">
                <div class="col-md-4">
                    <div class="cell">
                        <p class="name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</p>
                        <p class="term">Срок окупаемости: 14 месяцев</p>
                        <p class="old-price">324 000,00 (без НДС)</p>
                        <p class="price">270 000,00
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="11px" height="15px"
                                     version="1.1" shape-rendering="geometricPrecision"
                                     text-rendering="geometricPrecision" image-rendering="optimizeQuality"
                                     fill-rule="evenodd" clip-rule="evenodd"
                                     viewBox="0 0 336 436"
                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                 <g id="Слой_x0020_1">
                                  <metadata id="CorelCorpID_0Corel-Layer"/>
                                  <g id="_2461002018624">
                                   <path fill="#4D4D4D" fill-rule="nonzero"
                                         d="M47 436l0 -430c35,-4 85,-6 150,-6 52,0 88,9 109,28 20,19 30,55 30,107 0,52 -10,87 -30,106 -19,19 -54,28 -104,28 -33,0 -66,-1 -100,-5l0 172 -55 0zm55 -214l94 0c32,0 54,-6 66,-18 12,-12 18,-35 18,-69 0,-34 -6,-57 -18,-70 -13,-12 -35,-18 -66,-18 -46,0 -77,1 -94,1l0 174z"/>
                                   <line fill="none" stroke="#4D4D4D" stroke-width="29.4609" x1="0" y1="325" x2="209"
                                         y2="325"/>
                                  </g>
                                 </g>
                                </svg>
                            </span>
                        </p>
                        <p class="nds">(без НДС)</p>
                        <button class="btn-blue">КУПИТЬ</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="cell">
                        <p class="name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</p>
                        <p class="term">Срок окупаемости: 14 месяцев</p>
                        <p class="old-price">324 000,00 (без НДС)</p>
                        <p class="price">
                            270 000,00
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="11px" height="15px"
                                     version="1.1" shape-rendering="geometricPrecision"
                                     text-rendering="geometricPrecision" image-rendering="optimizeQuality"
                                     fill-rule="evenodd" clip-rule="evenodd"
                                     viewBox="0 0 336 436"
                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                 <g id="Слой_x0020_1">
                                  <metadata id="CorelCorpID_0Corel-Layer"/>
                                  <g id="_2461002018624">
                                   <path fill="#4D4D4D" fill-rule="nonzero"
                                         d="M47 436l0 -430c35,-4 85,-6 150,-6 52,0 88,9 109,28 20,19 30,55 30,107 0,52 -10,87 -30,106 -19,19 -54,28 -104,28 -33,0 -66,-1 -100,-5l0 172 -55 0zm55 -214l94 0c32,0 54,-6 66,-18 12,-12 18,-35 18,-69 0,-34 -6,-57 -18,-70 -13,-12 -35,-18 -66,-18 -46,0 -77,1 -94,1l0 174z"/>
                                   <line fill="none" stroke="#4D4D4D" stroke-width="29.4609" x1="0" y1="325" x2="209"
                                         y2="325"/>
                                  </g>
                                 </g>
                                </svg>
                            </span>
                        </p>
                        <p class="nds">(без НДС)</p>
                        <button class="btn-blue">КУПИТЬ</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="cell">
                        <p class="name">Сервер GsmSoft-GS-E200 для производства Ethereum (200 MH/S) 1100Вт/ч</p>
                        <p class="term">Срок окупаемости: 14 месяцев</p>
                        <p class="old-price">324 000,00 (без НДС)</p>
                        <p class="price">270 000,00
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="11px" height="15px"
                                     version="1.1" shape-rendering="geometricPrecision"
                                     text-rendering="geometricPrecision" image-rendering="optimizeQuality"
                                     fill-rule="evenodd" clip-rule="evenodd"
                                     viewBox="0 0 336 436"
                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                 <g id="Слой_x0020_1">
                                  <metadata id="CorelCorpID_0Corel-Layer"/>
                                  <g id="_2461002018624">
                                   <path fill="#4D4D4D" fill-rule="nonzero"
                                         d="M47 436l0 -430c35,-4 85,-6 150,-6 52,0 88,9 109,28 20,19 30,55 30,107 0,52 -10,87 -30,106 -19,19 -54,28 -104,28 -33,0 -66,-1 -100,-5l0 172 -55 0zm55 -214l94 0c32,0 54,-6 66,-18 12,-12 18,-35 18,-69 0,-34 -6,-57 -18,-70 -13,-12 -35,-18 -66,-18 -46,0 -77,1 -94,1l0 174z"/>
                                   <line fill="none" stroke="#4D4D4D" stroke-width="29.4609" x1="0" y1="325" x2="209"
                                         y2="325"/>
                                  </g>
                                 </g>
                                </svg>
                            </span>
                        </p>
                        <p class="nds">(без НДС)</p>
                        <button class="btn-blue">КУПИТЬ</button>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <section class="block-advantages">
        <div class="container">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="column-title">
                            Преимущества работы с нами
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="column-title">
                            Проблемы, возникающие при самостоятельной
                            работе
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Наши сервера являются уникальным лицензированным оборудованием не имеющим аналогов. При
                            производстве данных серверов наша компания работает только с надежными, проверенными
                            поставщиками, которые отвечают за свой продукт качеством и надежностью
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Существуют недобросовестные поставщики. Например, покупая мощную, дорогостоящую видеокарту
                            для
                            добычи - уверены ли Вы в том, что она не использовалась самими поставщиками для майнинга?
                            Возможно ей осталось работать совсем немного? Гарантия? Какие сроки гарантийного ремонта
                            купленного оборудования?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Максимальный срок доставки сервера, включая растаможку до 30 календарных дней экономит Ваше
                            время
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Сколько времени Вы будете ждать поставку?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Наши специалисты самостоятельно установят Ваш сервер в стоко-место в наш Дата-Центр,
                            подключат к
                            "сети", а так же выполнят качественную настройку для максимального результата. Наши
                            специалисты
                            это делают 24/7/365, предоставля качественный сервис
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Как Вы будете разбираться в этом? Как обслуживать дорогостоящее оборудование?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Наша дорогостоящая система пожарной безопасности отсекает непредвиденные риски возгорания
                            Вашей
                            техники и дает Вам надежность
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Как вы позаботитесь о системе пожарной безопасности?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Питание Ваших серверов остается всегда стабильным, благодаря мощным дорогостоящим
                            генераторам и
                            установленным высокоемким источникам бесперебойного питания
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            На какой энергии будет работать Ваше оборудование?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Гарантия на сервера составляет 12 месяцев. Благодаря данному гарантийному сроку вы полностью
                            окупаете Ваши затраты на покупку, вне зависимости от выбранного Вами варианта добычи. Мы
                            исключаем выход из строя Вашего оборудования. При данном событии мы переключим "сеть" Вашего
                            сервера на резервный, с аналогичными характеристиками
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Предоставляют ли Вам гарантию на сервера для майнинга?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Эксплуатационный срок обородования составляет 36 месяцев. Вы зарабатываете в течение 3 лет
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Каков срок работы Вашего оборудования?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Мы сотрудничаем с крупной финансовой компанией, имеющей платежную систему "SchonPay".
                            Обслуживаясь у нас, добытая криптовалюта Вашими сервереми, зачисляется каждые 3 часа на
                            карту
                            Visa, выпуск которой осуществлен также у нас
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Куда будет поступать ваша криптовалюта?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 delimiter">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Мы окажем Вам техническую поддержку и ответим на интерисующие Вас вопросы 24/7/365
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            Куда Вы обратитесь в случае возникновения вопросов?
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-6">
                        <p class="advantage">
                            Мы сотрудничаем с крупной компанией по производству сверхточных микросхем и программного
                            обеспечения GSM Soft
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            У Вас есть проверенные поставщики?
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="block-reviews">
        <div class="container">
            <div class="row">
                <div class="col-md-6">

                    <div class="desk-block">

                        <div class="desk-block-title">
                            <span>Отзывы</span>
                            <div class="nav-btns">
                                <button class="btn-prev1" id="btn-prev1"></button>
                                <button class="btn-next1" id="btn-next2"></button>
                            </div>
                        </div>
                        <div class="owl-carousel slider1 owl-theme">
                            <div>

                                <div class="desk comment-desk">

                                    <div class="col-sm-2">
                                        <div class="photo">
                                            <img src="images/system/2.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="title">
                                            <p>
                                                <span class="name">Alex,</span> VP of business development<br>
                                                <span class="from">from Innosilicon</span>
                                            </p>
                                        </div>
                                        <p>
                                            Genesis Mining является одной из лидирующих компаний облачного майнинга и
                                            нашим надежным партнером. Приятно видеть, что они являются порядочным
                                            сервисом облачного майнинга, который открыто демонстрирует свои
                                            майнинг-фермы широкой общественности.
                                        </p>
                                    </div>

                                </div>
                            </div>
                            <div>

                                <div class="desk comment-desk">

                                    <div class="col-sm-2">
                                        <div class="photo">
                                            <img src="images/system/2.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="title">
                                            <p>
                                                <span class="name">Alex,</span> VP of business development<br>
                                                <span class="from">from Innosilicon</span>
                                            </p>
                                        </div>
                                        <p>
                                            Genesis Mining является одной из лидирующих компаний облачного майнинга и
                                            нашим надежным партнером. Приятно видеть, что они являются порядочным
                                            сервисом облачного майнинга, который открыто демонстрирует свои
                                            майнинг-фермы широкой общественности.
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <a href="reviews.php" class="more">Все отзывы</a>
                    </div>


                </div>

                <div class="col-md-6">

                    <div class="desk-block">

                        <div class="desk-block-title">
                            <span>Полезная информация</span>
                            <div class="nav-btns">
                                <button class="btn-prev2" id="btn-prev2"></button>
                                <button class="btn-next2" id="btn-next2"></button>
                            </div>
                        </div>
                        <div class="owl-carousel slider2 owl-theme">
                            <div>
                                <div class="desk preview-new">

                                    <div class="col-xs-12">
                                        <div class="title">
                                            <p>
                                                <span class="name">Биткойны: технология, которая будет жить вечно</span><span
                                                        class="date">/17.01.2017</span>
                                            </p>
                                        </div>
                                        <p>
                                            Как и другие любители биткойнов, я обожаю говорить на эту тему. Я говорю об
                                            этом с пассажирами, которые летят со мной в самолете, с официантами, которые
                                            обслуживают меня в ресторанах. Не то, чтобы я намеренно навязывал это людям,
                                            просто эта тема естественно появляется в разговоре, и мне всегда...
                                        </p>
                                    </div>

                                </div>
                            </div>
                            <div>
                                <div class="desk preview-new">

                                    <div class="col-xs-12">
                                        <div class="title">
                                            <p>
                                                <span class="name">Биткойны: технология, которая будет жить вечно</span><span
                                                        class="date">/17.01.2017</span>
                                            </p>
                                        </div>
                                        <p>
                                            Как и другие любители биткойнов, я обожаю говорить на эту тему. Я говорю об
                                            этом с пассажирами, которые летят со мной в самолете, с официантами, которые
                                            обслуживают меня в ресторанах. Не то, чтобы я намеренно навязывал это людям,
                                            просто эта тема естественно появляется в разговоре, и мне всегда...
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <a href="blog.php" class="more">Все новости</a>

                    </div>


                </div>
            </div>
        </div>
    </section>
    <section class="footer">
        <?php
        include('footer.html')
        ?>
    </section>
</div>