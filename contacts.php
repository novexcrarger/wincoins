<?php
include ('header.html')
?>

<div class="contacts">
    <div class="container">
        <div class="row">
            <h2 class="text-center">
                Контактная информация
            </h2>
            <div class="col-xs-12 content">
                <div class="row">

                </div>
                <div class="col-md-3 col-md-offset-3">
                    <p>
                        162600<br>
                        Вологодская область, г. Череповец<br>
                        ул. Коммунистов, д. 25<br>
                        офис 305<br>
                    </p>

                </div>
                <div class="col-md-3">
                    <p>
               <span class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="19px" height="15px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"
                         viewBox="0 0 955 743"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                     <g id="Слой_x0020_1">
                      <metadata id="CorelCorpID_0Corel-Layer"/>
                      <path fill="#AAABAD" fill-rule="nonzero" d="M955 619c0,22 -6,42 -16,60l-301 -350 298 -271c12,19 19,42 19,66l0 495zm-478 -227l415 -376c-17,-10 -36,-16 -57,-16l-716 0c-20,0 -40,6 -57,16l415 376zm116 -23l-96 88c-6,5 -13,7 -20,7 -7,0 -14,-2 -19,-7l-96 -88 -306 355c19,12 40,19 63,19l716 0c23,0 45,-7 63,-19l-305 -355zm-574 -311c-12,19 -19,42 -19,66l0 495c0,22 6,42 15,60l302 -350 -298 -271z"/>
                     </g>
                    </svg>
               </span>Email: sales@genesis-mining.com</p>
                    <p>
               <span class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="16px" height="16px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"
                     viewBox="0 0 693 693"
                     xmlns:xlink="http://www.w3.org/1999/xlink">
                 <g id="Слой_x0020_1">
                  <metadata id="CorelCorpID_0Corel-Layer"/>
                  <path fill="#AAABAD" fill-rule="nonzero" d="M664 397c3,-17 4,-33 4,-50 0,-178 -144,-322 -321,-322 -18,0 -34,1 -51,4 -29,-18 -64,-29 -102,-29 -107,0 -194,87 -194,194 0,38 11,73 29,102 -3,17 -4,33 -4,51 0,177 144,321 322,321 17,0 33,-1 50,-4 29,18 64,29 102,29 107,0 194,-87 194,-194 0,-38 -11,-73 -29,-102zm-165 96c-13,19 -33,35 -59,46 -27,11 -58,16 -93,16 -43,0 -79,-7 -106,-22 -20,-11 -36,-26 -49,-44 -12,-18 -19,-36 -19,-53 0,-10 4,-19 12,-26 8,-8 17,-11 29,-11 10,0 18,2 24,8 7,6 13,14 17,25 6,13 12,23 18,32 6,8 15,15 26,20 12,6 27,9 45,9 26,0 47,-6 63,-17 16,-11 23,-24 23,-41 0,-13 -4,-23 -12,-31 -9,-8 -20,-14 -34,-19 -13,-4 -32,-9 -55,-14 -31,-6 -57,-14 -78,-23 -21,-9 -38,-22 -51,-38 -12,-15 -18,-35 -18,-59 0,-22 6,-42 19,-59 14,-18 33,-31 57,-40 25,-9 54,-14 88,-14 26,0 49,3 69,9 19,6 35,14 48,25 13,10 23,21 29,32 6,11 9,22 9,33 0,10 -4,19 -12,27 -7,8 -17,12 -28,12 -11,0 -19,-2 -24,-7 -6,-5 -11,-12 -17,-23 -8,-15 -17,-27 -28,-35 -11,-8 -28,-13 -52,-13 -23,0 -41,5 -54,14 -14,9 -20,20 -20,32 0,8 2,14 6,20 5,5 11,10 19,15 9,4 17,7 25,9 9,3 23,6 42,11 24,5 46,11 66,17 20,7 37,14 51,24 14,9 25,21 33,35 8,14 12,31 12,52 0,24 -7,46 -21,66z"/>
                 </g>
                </svg>
               </span>Skype: sales@genesis-mining.com</p>
                    <p>
               <span class="icon">
               <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="16px" height="16px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"
                    viewBox="0 0 684 684"
                    xmlns:xlink="http://www.w3.org/1999/xlink">
                 <g id="Слой_x0020_1">
                  <metadata id="CorelCorpID_0Corel-Layer"/>
                  <g id="_2425212459872">
                   <path fill="#AAABAD" fill-rule="nonzero" d="M0 684l59 -176c-30,-50 -47,-109 -47,-172 0,-185 150,-336 336,-336 186,0 336,151 336,336 0,186 -150,337 -336,337 -56,0 -110,-14 -156,-39l-192 50z"/>
                   <path fill="whitesmoke" fill-rule="nonzero" d="M563 448c-16,-29 -73,-63 -73,-63 -13,-7 -29,-8 -36,5 0,0 -19,23 -23,26 -22,14 -42,14 -62,-6l-94 -94c-20,-20 -21,-41 -6,-62 3,-5 25,-23 25,-23 13,-8 12,-24 5,-37 0,0 -34,-57 -63,-72 -12,-7 -28,-5 -38,5l-20 21c-66,66 -34,140 32,206l120 120c66,66 140,99 206,33l21 -21c10,-10 12,-25 6,-38z"/>
                  </g>
                 </g>
                </svg>
               </span>WhatsApp: +7 900-000-00-00</p>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div id="map">
                        <div class="contact-form">
                            <h4 class="title">Обращение в службу<br>
                                поддержки</h4>
                            <textarea name="" id="" cols="30" rows="10"></textarea>
                            <a href="">
                                <p><span class="add-file">+</span>  Прикрепить файл</p>
                            </a>
                            <div class="text-center check">

                                <label>
                                    <input class="checkbox" type="radio" value="1">
                                    <span class="checkbox-custom"></span>Согласен на обработку персональных данных

                                </label>
                            </div>

                            <button class="btn-orange">Отправить</button>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php
include ('footer.html')
?>